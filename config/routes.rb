Rails.application.routes.draw do
 
  mount Ckeditor::Engine => '/ckeditor'
  root 'static_pages#home'
  get '/about',           to: 'static_pages#about'
  get '/contact',         to: 'static_pages#contact'
  get '/login',           to: 'sessions#new'
  post '/login',          to: 'sessions#create'
  delete '/logout',       to: 'sessions#destroy'
  # Incase new users need to be created
  #get  '/signup',         to: 'users#new'
  #post '/signup',         to: 'users#create'
  resources :users
  resources :posts
  resources :categories
  resources :products
end
