require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category = categories(:one)
    @adminuser = users(:one)
    @user = users(:two)
  end

  test "should get index" do
    get categories_url
    assert_response :success
  end

  test "should get new" do
    log_in_as(@adminuser)
    get new_category_url
    assert_response :success
  end

  test "should create category" do
    assert_difference('Category.count') do
      post categories_url, params: { category: { name: @category.name } }
    end

    assert_redirected_to categories_url
  end

  test "should show category" do
    get category_url(@category)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@adminuser)
    get edit_category_url(@category)
    assert_response :success
  end

  test "should update category" do
    patch category_url(@category), params: { category: { name: @category.name } }
    assert_redirected_to category_url(@category)
  end

  test "should destroy category" do
    log_in_as(@adminuser)
    assert_difference('Category.count', -1) do
      delete category_url(@category)
    end

    assert_redirected_to categories_url
  end
  
  test "shouldn't destroy category uncategorized" do
    log_in_as(@adminuser)
    @category = categories(:uncategorized)
    assert_no_difference('Category.count') do
      delete category_url(@category)
    end

    assert_redirected_to categories_url
  end
  
  test "non admin user should not be able to create new category"do 
    log_in_as(@user)
    get new_category_url
    assert_redirected_to login_path
  end
  
  test "non admin user should not be able to edit category"do 
    log_in_as(@user)
    get edit_category_url(@category)
    assert_redirected_to login_path
  end
  
  test "non admin user should not be able to delete category"do 
    log_in_as(@user)
    delete category_url(@category)
    assert_redirected_to login_path
  end
  
end
