require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
    @adminuser = users(:one)
    @user = users(:two)
  end

  test "should get index" do
    get posts_url
    assert_response :success
  end

  test "should get new" do
    log_in_as(@adminuser)
    get new_post_url
    assert_response :success
  end

  test "should create category" do
    assert_difference('Post.count') do
      post posts_url, params: { post: { title: @post.title, body: @post.body, description: @post.description, category_id: @post.category_id } }
    end

    assert_redirected_to post_url(Post.last)
  end

  test "should show post" do
    get post_url(@post)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@adminuser)
    get edit_post_url(@post)
    assert_response :success
  end

  test "should update post" do
    log_in_as(@adminuser)
    patch post_url(@post), params: { post: { title: @post.title, body: @post.body, description: @post.description, category_id: @post.category_id } }
    assert_redirected_to post_url(@post)
  end

  test "should destroy post" do
    log_in_as(@adminuser)
    assert_difference('Post.count', -1) do
      delete post_url(@post)
    end

    assert_redirected_to posts_url
  end

  test "Non admin user should not be able to create product" do
    log_in_as(@user)
    get new_post_url
    assert_redirected_to login_path
  end
  
  test "Non admin user should not be able to edit product" do
    log_in_as(@user)
    get edit_post_url(@post)
    assert_redirected_to login_path
  end
  
  test "Non admin user should not be able to delete post" do
    log_in_as(@user)
    delete post_url(@post)
    assert_redirected_to login_path
  end
  
end
