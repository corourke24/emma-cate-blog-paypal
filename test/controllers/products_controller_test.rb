require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product = products(:one)
    @adminuser = users(:one)
    @user = users(:two)
  end

  test "should get index" do
    get products_url
    assert_response :success
  end

  test "should get new" do
    log_in_as(@adminuser)
    get new_product_url
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post products_url, params: { product: { title: @product.title, price: @product.price, description: @product.description, paypalbutton: @product.paypalbutton, active: @product.active } }
    end

    assert_redirected_to product_url(Product.last)
  end

  test "should show product" do
    get product_url(@product)
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@adminuser)
    get edit_product_url(@product)
    assert_response :success
  end

  test "should update product" do
    log_in_as(@adminuser)
    updated_title = "new title"
    patch product_path(@product), params: { product: { title: updated_title, price: @product.price, description: @product.description, paypalbutton: @product.paypalbutton, active: @product.active } }
    assert_redirected_to product_url(@product)
    @product.reload
      assert_equal updated_title, @product.title
  end

  test "should destroy product" do
    log_in_as(@adminuser)
    assert_difference('Product.count', -1) do
      delete product_url(@product)
    end

    assert_redirected_to products_url
  end
  
  test "paypalbutton shouldn't be visible if product inactive" do
    patch product_url(@product), params: { product: { title: @product.title, price: @product.price, description: @product.description, paypalbutton: @product.paypalbutton, active: false } }
    assert_redirected_to product_url(@product)
    @product.reload
    assert_equal false, @product.active
  end
  
  test "Non admin user should not be able to create product" do
    log_in_as(@user)
    get new_product_url
    assert_redirected_to login_path
  end
  
  test "Non admin user should not be able to edit product" do
    log_in_as(@user)
    get edit_product_url(@product)
    assert_redirected_to login_path
  end
    
  test "Non admin user should not be able to delete product" do
    log_in_as(@user)
    delete product_url(@product)
    assert_redirected_to login_path
  end
end
