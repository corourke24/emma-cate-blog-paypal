class AddPricePaypalbuttonAndPictureToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :price, :decimal
    add_column :products, :paypalbutton, :text
    add_column :products, :picture, :string
  end
end
