class AddDescriptionAndTitleToProducts < ActiveRecord::Migration[5.0]
  def change
     add_column :products, :title, :string
     add_column :products, :description, :text
  end
end
