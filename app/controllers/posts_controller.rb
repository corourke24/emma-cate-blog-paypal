class PostsController < ApplicationController
 
  # Index action to render all posts
  def index
    @posts = Post.all.order(created_at: :desc).paginate(page: params[:page], :per_page => 10)
  end

  # New action for creating post
  def new
    if logged_in? && current_user.admin? 
      @post = Post.new
      @categories = Category.all.map{|c| [ c.name, c.id ] }
    else
      redirect_to login_path
    end
  end

  # Create action saves the post into database
  def create
    @post = Post.new(post_params)
    @post.category_id = params[:category_id]
    if @post.save(post_params)
      flash[:notice] = "Successfully created post!"
      redirect_to post_path(@post)
    else
      flash[:alert] = "Error creating new post!"
      render :new
    end
  end

  # Edit action retrives the post and renders the edit page
  def edit
    if logged_in? && current_user.admin? 
      @post = Post.find(params[:id])
      @categories = Category.all.map{|c| [ c.name, c.id ] }
    else
      redirect_to login_path
    end
  end

  # Update action updates the post with the new information
  def update
    @post = Post.find(params[:id])
    @post.category_id = params[:category_id]
    if @post.update_attributes(post_params)
      flash[:notice] = "Successfully updated post!"
      redirect_to post_path(@post)
    else
      flash[:alert] = "Error updating post!"
      render :edit
    end
  end

  # The show action renders the individual post after retrieving the the id
  def show
    @post = Post.find(params[:id])
  end
  
  # The destroy action removes the post permanently from the database
  def destroy
    if logged_in? && current_user.admin? 
      @post = Post.find(params[:id])
      if @post.destroy
        flash[:notice] = "Successfully deleted post!"
        redirect_to posts_path
      else
        flash[:alert] = "Error updating post!"
      end
    else
      redirect_to login_path
    end
  end
  
  private

  def post_params
    params.require(:post).permit(:title, :body, :description, :category_id)
  end
   
end
