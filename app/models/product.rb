class Product < ApplicationRecord
    mount_uploader :picture, PictureUploader
    validates :title,  presence: true, length: { maximum: 300 }
    PRICE_REGEXP = /\A\d{1,4}(\.\d{0,2})?\z/
    validates :price, presence: true,
                      numericality: true,
                      format: { with: PRICE_REGEXP }
    validates :description, presence: true, length: { maximum: 1000 }, 
                            allow_nil: true
    validate :picture_size
    
    private
    
        def picture_size
            if picture.size > 5.megabytes
                errors.add(:picture, "must be less than 5MB")
            end
        end
end
