class User < ApplicationRecord
    has_many :posts, dependent: :destroy
    attr_accessor :remember_token
    before_save :downcase_email
    validates :name,  presence: true, length: { maximum: 50 }
    EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 245 },
                      format: { with: EMAIL_REGEX },
                      uniqueness: { case_sensitive: false }
    has_secure_password 
    validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
    
    class << self
        # Returns hash digest of given string
        def digest(s)
            cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                        BCrypt::Engine.cost
            BCrypt::Password.create(s, cost: cost)
        end
    
        # Returns random token
        def new_token
            SecureRandom.urlsafe_base64
        end
    end
    
    # Remembers user in database for persistent sessions
    def remember
        self.remember_token = User.new_token
        update_attribute(:remember_digest, User.digest(remember_token))
    end
    
   
    
    # Forgets a user
    def forget
        update_attribute(:remember_digest, nil)
    end
    
    private 
    
        def downcase_email
            self.email = email.downcase
        end
end
